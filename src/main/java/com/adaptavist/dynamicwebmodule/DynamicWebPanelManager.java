package com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Used to register web-panels on plugin startup. Each panel (vm file) must be in a directory inside a 'web-panel'
 * directory in the plugin resource paths.
 */
@Component
@ExportAsDevService
public class DynamicWebPanelManager extends AbstractDynamicResourceModuleManager {

    public static final String WEB_PANEL_PATH = "web-panel";
    public static final String WEB_PANEL_ELEMENT = "web-panel";
    public static final String VIEW_NAME = "view";
    public static final String VELOCITY_TYPE = "velocity";
    public static final String VELOCITY_EXTENSION = ".vm";

    @Autowired
    public DynamicWebPanelManager(DynamicModuleDescriptorFactory moduleDescriptorFactory,
                                  @ComponentImport PluginAccessor pluginAccessor,
                                  BundleContext bundleContext) {
        super(moduleDescriptorFactory, pluginAccessor, bundleContext);
    }

    @Override
    protected String getResourcePath() {
        return WEB_PANEL_PATH;
    }

    @Override
    protected Iterable<Element> getModuleElements(Plugin plugin, Bundle bundle) {
        return Iterables.transform(findResources(bundle, "*" + VELOCITY_EXTENSION, true), new Function<String, Element>() {
            @Override
            public Element apply(String path) {
                final Document document = DocumentHelper.createDocument();

                document.addElement(WEB_PANEL_ELEMENT)
                        .addAttribute(KEY, getKey(path))
                        .addAttribute(LOCATION, getRootPath(path))
                        .addElement(RESOURCE_ELEMENT)
                        .addAttribute(NAME_ATTRIBUTE, VIEW_NAME)
                        .addAttribute(TYPE_ATTRIBUTE, VELOCITY_TYPE)
                        .addAttribute(LOCATION, path);

                return document.getRootElement();
            }
        });
    }
}
