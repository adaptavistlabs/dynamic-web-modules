package com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.adaptavist.dynamicwebmodule.resources.WebResourcesBuilder;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.atlassian.pocketknife.api.lifecycle.modules.ModuleRegistrationHandle;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterators;
import org.dom4j.Element;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static org.apache.commons.lang.StringUtils.substringAfterLast;
import static org.apache.commons.lang.StringUtils.substringBeforeLast;

/**
 * Class that automatically registers web-resources/web-panels on plugin startup.
 * Provides methods to register an Element to this plugin and also to unregister modules from this plugin using the
 * element key (see getHandleKey).
 * Contains methods that would allow registering and unregistering to other plugins that could be made public in another
 * release.
 */
public abstract class AbstractDynamicResourceModuleManager implements InitializingBean, DisposableBean {

    public static final String LOCATION = "location";
    public static final String KEY = "key";
    public static final String RESOURCE_ELEMENT = "resource";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String LOCATION_ATTRIBUTE = "location";
    public static final String TYPE_ATTRIBUTE = "type";
    public static final String DOWNLOAD_TYPE = "download";
    public static final String SOY_TYPE = "soy";

    public static final Joiner PATH_JOINER = Joiner.on('/');
    public static final Splitter PATH_SPLITTER = Splitter.on('/').omitEmptyStrings();

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected final DynamicModuleDescriptorFactory moduleDescriptorFactory;
    protected final PluginAccessor pluginAccessor;
    protected final BundleContext bundleContext;

    protected Map<String, ModuleRegistrationHandle> registrationHandles = new ConcurrentHashMap<>();

    public AbstractDynamicResourceModuleManager(DynamicModuleDescriptorFactory moduleDescriptorFactory,
                                                @ComponentImport PluginAccessor pluginAccessor,
                                                BundleContext bundleContext) {
        this.moduleDescriptorFactory = moduleDescriptorFactory;
        this.pluginAccessor = pluginAccessor;
        this.bundleContext = bundleContext;
    }

    @Override
    public void afterPropertiesSet() {
        addModules(bundleContext.getBundle());
    }

    @Override
    public void destroy() {
        for (String key : registrationHandles.keySet()) {
            registrationHandles.get(key).unregister();
        }
        registrationHandles.clear();
    }

    // Public API

    public void registerModuleElement(Element element) {
        final String pluginKey = getPluginKey();

        if (pluginKey != null) {
            final Plugin plugin = pluginAccessor.getPlugin(pluginKey);
            registerModuleElement(plugin, element);
        }
        logRegHandles();
    }

    public void unregisterModuleElement(String elementKey) {
        final String pluginKey = getPluginKey();

        if (pluginKey != null) {
            final Plugin plugin = pluginAccessor.getPlugin(pluginKey);
            unregisterModuleElement(plugin, elementKey);
        }
        logRegHandles();
    }

    // Inner methods

    protected void unregisterModuleElement(Plugin plugin, String elementKey) {
        final String handleKey = getHandleKey(plugin, elementKey);
        registrationHandles.get(handleKey).unregister();
        registrationHandles.remove(handleKey);

        log.debug("Unregistered dynamic module '{}' for plugin '{}'", elementKey, plugin.getKey());
    }

    protected String getHandleKey(Plugin plugin, String elementKey) {
        return plugin.getKey() + ":" + elementKey;
    }

    protected void registerModuleElement(Plugin plugin, Element element) {
        final ModuleRegistrationHandle handle = moduleDescriptorFactory.loadModules(plugin, element);
        final String handleKey = getHandleKey(plugin, element.attributeValue("key"));
        registrationHandles.put(handleKey, handle);

        log.debug("Registered dynamic module:\n{}", element.asXML());
    }

    protected void addModules(Bundle bundle) {
        final String pluginKey = getPluginKey(bundle);

        if (pluginKey != null) {
            final Plugin plugin = pluginAccessor.getPlugin(pluginKey);

            for (Element element : getModuleElements(plugin, bundle)) {
                registerModuleElement(plugin, element);
            }
        }
    }

    protected String getPluginKey() {
        return getPluginKey(bundleContext.getBundle());
    }

    protected String getPluginKey(Bundle bundle) {
        return (String) bundle.getHeaders().get(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
    }

    // Abstract methods

    protected abstract Iterable<Element> getModuleElements(Plugin plugin, Bundle bundle);

    protected abstract String getResourcePath();


    // Utility methods

    protected Iterable<String> findResources(final Bundle bundle, final String filePattern, final boolean recurse) {
        return new Iterable<String>() {
            @Override
            public Iterator<String> iterator() {
                final Enumeration<URL> entries = bundle.findEntries(getResourcePath(), filePattern, recurse);
                if (entries == null) {
                    return new ArrayList<String>().iterator();
                } else {
                    return Iterators.transform(Iterators.forEnumeration(entries), new Function<URL, String>() {
                        @Override
                        public String apply(URL url) {
                            return url.getPath();
                        }
                    });
                }
            }
        };
    }

    public WebResourcesBuilder getWebResourcesBuilder() {
        final String pluginKey = getPluginKey();

        if (pluginKey != null) {
            final Plugin plugin = pluginAccessor.getPlugin(pluginKey);
            return WebResourcesBuilder.forPlugin(plugin);
        } else {
            throw new NullPointerException(String.format("Could not get the '%s' header value from the bundle context so" +
                    " could not get the plugin in order to return a WebResourcesBuilder", OsgiPlugin.ATLASSIAN_PLUGIN_KEY));
        }
    }

    public Set<String> getRegisteredModuleKeys() {
        return registrationHandles.keySet();
    }

    public static String getName(String path) {
        return substringAfterLast(path, "/");
    }

    public static String getKey(String path) {
        return getRootPath(path).replace('/', '-').replaceAll("^-", "");
    }

    public static String getRootPath(String path) {
        return substringBeforeLast(path, "/");
    }

    protected void logRegHandles() {
        log.debug("Reg handles: {}", registrationHandles.keySet());
    }

}
