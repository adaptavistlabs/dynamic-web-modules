package com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.adaptavist.dynamicwebmodule.resources.WebResourcesBuilder;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import org.dom4j.Element;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Used to register web-resources on plugin startup. Each resource must be in a directory inside a 'web-resource'
 * directory in the plugin resource paths.
 */
@Component
@ExportAsDevService
public class DynamicWebResourceManager extends AbstractDynamicResourceModuleManager {

    public static final String WEB_RESOURCE_PATH = "web-resource";

    @Autowired
    public DynamicWebResourceManager(DynamicModuleDescriptorFactory moduleDescriptorFactory,
                                     @ComponentImport PluginAccessor pluginAccessor,
                                     BundleContext bundleContext) {
        super(moduleDescriptorFactory, pluginAccessor, bundleContext);
    }

    @Override
    protected String getResourcePath() {
        return WEB_RESOURCE_PATH;
    }

    @Override
    protected Iterable<Element> getModuleElements(Plugin plugin, Bundle bundle) {

        final WebResourcesBuilder builder = WebResourcesBuilder.forPlugin(plugin);

        for (String path : findResources(bundle, "*.*", true)) {
            builder.addResource(path);
        }

        return builder.getElements();
    }
}
