package com.adaptavist.dynamicwebmodule.resources;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.plugin.Plugin;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static com.adaptavist.dynamicwebmodule.AbstractDynamicResourceModuleManager.*;
import static org.apache.commons.lang.StringUtils.endsWith;
import static org.apache.commons.lang.StringUtils.substringBeforeLast;

/**
 * For creating web-resources for a plugin and ensuring soy and less transformers are included.
 * If there is no web-resource.xml in the same directory as the resource path being added (or it can't be found/read)
 * then a new one is generated.
 */
public class WebResourcesBuilder {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public static final String WEB_RESOURCE_ELEMENT = "web-resource";
    public static final String DESCRIPTOR_FILE = "web-resource.xml";

    public static final String TRANSFORMATION_ELEMENT = "transformation";
    public static final String TRANSFORMER_ELEMENT = "transformer";
    public static final String EXTENSION_ATTRIBUTE = "extension";

    private static final Set<String> STRIP_ELEMENTS = ImmutableSet.of("before", "after");

    final Plugin plugin;
    final Map<String, WebResourceBuilder> webResourceBuilders = new HashMap<>();

    private WebResourcesBuilder(Plugin plugin) {
        this.plugin = plugin;
    }

    public static WebResourcesBuilder forPlugin(Plugin plugin) {
        return new WebResourcesBuilder(plugin);
    }

    public void addResource(String path) {
        final WebResourceBuilder builder = getWebResourceBuilder(path);

        if (!isExcluded(path)) {

            // TODO: AVST-659 - Consider making these pluggable in the future

            if (path.endsWith(".soy")) {
                builder.addSoy(path);
            } else if (path.endsWith(".less")) {
                builder.addLess(path);
            } else {
                builder.addOther(path);
            }
        }
    }

    private boolean isExcluded(String path) {
        return endsWith(path, "-min.js") || endsWith(path, "/" + DESCRIPTOR_FILE);
    }

    private WebResourceBuilder getWebResourceBuilder(String path) {
        final String key = getKey(path);
        WebResourceBuilder builder = webResourceBuilders.get(key);

        // TODO: This should probably be implemented as a CacheLoader
        if (builder == null) {
            builder = new WebResourceBuilder(key, path);
            webResourceBuilders.put(key, builder);
        }
        return builder;
    }

    private void expandDependencies(List<Element> dependencies) {
        for (Element dependency : dependencies) {
            final String moduleKey = dependency.getTextTrim();
            if (moduleKey.startsWith(":")) {
                // Prepend with the plugin key
                dependency.setText(plugin.getKey() + moduleKey);
            }
        }
    }

    private Element loadDescriptor(String rootPath) {
        final InputStream descriptor = plugin.getResourceAsStream(rootPath + '/' + DESCRIPTOR_FILE);
        if (descriptor != null) {
            try {
                final SAXReader reader = new SAXReader();
                final Document document = reader.read(descriptor);
                return document.getRootElement();
            } catch (DocumentException e) {
                log.error("Failed to read WebResource descriptor", e);
            }
        }
        return null;
    }

    public Iterable<Element> getElements() {
        return ImmutableList.copyOf(Collections2.transform(webResourceBuilders.values(), new Function<WebResourceBuilder, Element>() {
            @Override
            public Element apply(WebResourceBuilder builder) {
                return builder.build();
            }
        }));
    }

    private class WebResourceBuilder {

        @Nonnull
        private final Element root;

        @Nonnull
        private final Map<String, Element> befores;

        @Nonnull
        private final Map<String, Element> afters;

        @Nullable
        private final Element othersBeforeThis;

        public WebResourceBuilder(String key, String path) {
            Element element = loadDescriptor(getRootPath(path));

            if (element != null) {
                expandDependencies(element.elements("dependency"));
                befores = findPlaceholders(element.elements("before"));
                afters = findPlaceholders(element.elements("after"));
            } else {
                final Document document = DocumentHelper.createDocument();
                element = document.addElement(WEB_RESOURCE_ELEMENT);
                befores = Collections.emptyMap();
                afters = Collections.emptyMap();
            }

            othersBeforeThis = Iterables.getFirst(afters.values(), null);

            if (element.attributeValue(KEY) == null) {
                element.addAttribute(KEY, key);
            }

            if (element.attributeValue(NAME_ATTRIBUTE) == null) {
                element.addAttribute(NAME_ATTRIBUTE, "Dynamic Web Resource: " + key);
            }

            root = element;
        }

        private Map<String, Element> findPlaceholders(List<Element> elements) {
            if (elements.isEmpty()) {
                return Collections.emptyMap();
            } else {
                final Map<String, Element> placeholders = new LinkedHashMap<>();
                for (Element element : elements) {
                    placeholders.put(element.getTextTrim(), element);
                }
                return placeholders;
            }
        }

        private void addSoy(String path) {
            final String name = getName(path);

            // Add transformer for compiling the soy to JS
            addTransformationElement("soy", "soyTransformer");

            // Add the downloadable compiled JS
            addResourceElement(path, name + ".js", DOWNLOAD_TYPE);

            // Add soy template resource for server-side usage
            addResourceElement(path, name, SOY_TYPE);
        }

        private void addLess(String path) {
            // Add transformer for compiling LESS to CSS
            addTransformationElement("less", "lessTransformer");

            // Add the downloadable transformed CSS
            addResourceElement(path, substringBeforeLast(getName(path), ".less") + ".css", DOWNLOAD_TYPE);
        }

        private void addOther(String path) {
            // Add the downloadable resource
            addResourceElement(path, getName(path), DOWNLOAD_TYPE);
        }

        private void addResourceElement(String path, String name, String type) {
            final Element resource = DocumentHelper.createElement(RESOURCE_ELEMENT)
                    .addAttribute(NAME_ATTRIBUTE, name)
                    .addAttribute(LOCATION_ATTRIBUTE, path)
                    .addAttribute(TYPE_ATTRIBUTE, type);

            final Element before = befores.get(name);
            final Element after = afters.get(name);

            if (before != null) {
                insertBefore(before, resource);
            } else if (after != null) {
                insertAfter(after, resource);
            } else if (othersBeforeThis != null) {
                insertBefore(othersBeforeThis, resource);
            } else {
                root.add(resource);
            }
        }

        private void insertBefore(Element before, Element resource) {
            final List elements = before.getParent().elements();
            elements.add(elements.indexOf(before), resource);
        }

        private void insertAfter(Element after, Element resource) {
            final List elements = after.getParent().elements();
            final int index = elements.indexOf(after) + 1;
            if (index >= elements.size()) {
                elements.add(resource);
            } else {
                elements.add(index, resource);
            }
        }

        private void addTransformationElement(String extension, String transformerKey) {
            if (!containsTransformation(extension)) {
                root.addElement(TRANSFORMATION_ELEMENT)
                        .addAttribute(EXTENSION_ATTRIBUTE, extension)
                        .addElement(TRANSFORMER_ELEMENT)
                        .addAttribute(KEY, transformerKey);
            }
        }

        private boolean containsTransformation(final String extension) {
            return root.elements(TRANSFORMATION_ELEMENT).stream().anyMatch(input -> {
                return input instanceof Element && ((Element) input).attributeValue(EXTENSION_ATTRIBUTE, "").equals(extension);
            });
        }

        public Element build() {
            stripElements();
            return root;
        }

        private void stripElements() {
            Iterators.removeIf(root.elements().iterator(), new Predicate() {
                @Override
                public boolean apply(Object input) {
                    return input instanceof Element && STRIP_ELEMENTS.contains(((Element) input).getName());
                }

                @Override
                public boolean test(@org.checkerframework.checker.nullness.qual.Nullable Object input) {
                    return true;
                }
            });
        }
    }


}
