package ut.com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.adaptavist.dynamicwebmodule.resources.WebResourcesBuilder;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.dom4j.Element;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AbstractDynamicResourceModuleManagerTest extends AbstractDynamicResourceTest {

    private Iterable<Element> elements;

    @Test
    public void testAddingResources() throws FileNotFoundException {

        given_dynamic_web_resource_manager();
        and_web_resources(
                "/web-resource/test-one/example.js",
                "/web-resource/test-two/example.html"
        );

        when_resources_registered();

        then_the_module_descriptor_factory_does_its_thing();

        and_resource_handles(hasItems(
                is(PLUGIN_KEY + ":web-resource-test-one"),
                is(PLUGIN_KEY + ":web-resource-test-two")
        ));
    }

    @Test
    public void testRemovingResources() throws FileNotFoundException {

        given_dynamic_web_resource_manager();
        and_web_resources_are_registered(
                "/web-resource/test-one/example.js",
                "/web-resource/test-two/example.soy"
        );

        when_resources_are_unregistered(
                "/web-resource/test-two"
        );

        then_resource_handles(hasItem(
                is(PLUGIN_KEY + ":web-resource-test-one")
        ));

        when_resources_are_unregistered(
                "/web-resource/test-one"
        );

        then_resource_handles(iterableWithSize(0));
    }

    private void when_resources_are_unregistered(String... paths) {
        for (String path : paths) {
            final String resourceKey = Joiner.on("-").join(
                    Splitter.on("/").omitEmptyStrings().split(path)
            );
            resourceModuleManager.unregisterModuleElement(resourceKey);
        }
    }

    private void and_web_resources_are_registered(String... paths) {
        and_web_resources(paths);
        when_resources_registered();
    }

    private void then_the_module_descriptor_factory_does_its_thing() {
        final int numElements = Iterables.size(elements);
        verify(moduleDescriptorFactory, times(numElements))
                .loadModules(eq(plugin), any(Element.class));
    }

    private void when_resources_registered() {
        for (Element elem : elements) {
            resourceModuleManager.registerModuleElement(elem);
        }
    }

    private void and_web_resources(String... paths) {
        final WebResourcesBuilder webResourcesBuilder = resourceModuleManager.getWebResourcesBuilder();
        for (String path : paths) {
            webResourcesBuilder.addResource(path);
        }
        elements = webResourcesBuilder.getElements();
    }

}
