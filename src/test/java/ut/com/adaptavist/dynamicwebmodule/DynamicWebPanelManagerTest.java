package ut.com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.hamcrest.Matchers.*;

public class DynamicWebPanelManagerTest extends AbstractDynamicResourceTest {

    @Test
    public void testAddModules() throws FileNotFoundException {
        given_dynamic_web_panel_manager();

        when_dynamic_web_panel_manager_initialized();

        then_resource_handles(hasItem(
                containsString("web-panel-sample-panel")
        ));

        then_resource_handles(iterableWithSize(1));
    }

    @Test
    public void testRemoveModules() throws FileNotFoundException {
        given_dynamic_web_resource_manager();
        and_dynamic_web_panel_manager_initialized();

        when_dynamic_web_panel_manager_destroyed();

        then_resource_handles(iterableWithSize(0));
    }

    private void when_dynamic_web_panel_manager_destroyed() {
        resourceModuleManager.destroy();
    }

    private void and_dynamic_web_panel_manager_initialized() {
        when_dynamic_web_panel_manager_initialized();
    }

    private void when_dynamic_web_panel_manager_initialized() {
        resourceModuleManager.afterPropertiesSet();
    }

}
