package ut.com.adaptavist.dynamicwebmodule.matchers;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.dom4j.Element;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Used to check Element contents (the actual files)
 */
public class HasContentsMatcher extends TypeSafeMatcher<Element> {

    private final Matcher matcher;

    private HasContentsMatcher(Matcher matcher) {
        this.matcher = matcher;
    }

    @Override
    protected boolean matchesSafely(Element item) {
        return matcher.matches(item.content());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("contents is ");
        matcher.describeTo(description);
    }

    @Factory
    public static <T> Matcher<Element> hasContents(Matcher matcher) {
        return new HasContentsMatcher(matcher);
    }
}
