package ut.com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.adaptavist.dynamicwebmodule.resources.WebResourcesBuilder;
import com.atlassian.plugin.Plugin;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.DOMWriter;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import ut.com.adaptavist.dynamicwebmodule.util.PluginWithResource;

import java.io.FileNotFoundException;
import java.util.Collections;

import static com.adaptavist.dynamicwebmodule.AbstractDynamicResourceModuleManager.PATH_JOINER;
import static com.adaptavist.dynamicwebmodule.AbstractDynamicResourceModuleManager.PATH_SPLITTER;
import static com.google.common.collect.Iterables.limit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static ut.com.adaptavist.dynamicwebmodule.matchers.HasAttrValueMatcher.hasAttrValue;
import static ut.com.adaptavist.dynamicwebmodule.matchers.HasContentsMatcher.hasContents;

public class WebResourcesBuilderTest {

    private static final String OUR_PLUGIN_KEY = "builder-test";

    private WebResourcesBuilder webResourcesBuilder;
    private Iterable<Element> webResourceElements;

    @Before
    public void before() {
        webResourcesBuilder = null;
        webResourceElements = Collections.emptyList();
    }

    @Test
    public void testWithNoElements() throws FileNotFoundException {
        given_web_resource_builder();

        when_i_get_elements();

        then_elements(hasSize(0));
    }

    @Test
    public void testMultipleElements() throws FileNotFoundException {
        given_web_resource_builder(
                web_resource("sample-resource-a/one.txt"),
                web_resource("sample-resource-b/two.txt")
        );

        when_i_get_elements();

        then_elements(hasSize(2));
    }

    @Test
    public void testWithAnElementKey() throws FileNotFoundException {
        given_web_resource_builder(
                web_resource("with-key/sample.js"),
                web_resource("with-key/sample.css")
        );

        when_i_get_elements();

        then_elements(hasSize(1));

        then_elements(hasItems(
                allOf(
                        hasAttrValue("key", "sample-resource"),
                        hasAttrValue("name", "Sample Resource"),
                        hasContents(
                                hasItems(
                                        hasAttrValue("name", "sample.js"),
                                        hasAttrValue("name", "sample.css")
                                )
                        )
                )
        ));
    }

    @Test
    public void testWithAnElementNoKey() throws FileNotFoundException {
        given_web_resource_builder(
                web_resource("without-key/sample.soy")
        );

        when_i_get_elements();

        then_elements(hasItems(
                allOf(
                        hasAttrValue("key", "web-resource-without-key"),
                        hasAttrValue("name", "No Key Resource")
                )
        ));
    }

    @Test
    public void testWithAnElementNoXML() throws FileNotFoundException {
        given_web_resource_builder(
                web_resource("without-xml/sample.css"),
                web_resource("without-xml/sample.js")
        );

        when_i_get_elements();

        then_elements(hasItems(
                allOf(
                        hasAttrValue("key", "web-resource-without-xml"),
                        hasAttrValue("name", "Dynamic Web Resource: web-resource-without-xml"),
                        hasContents(
                                hasItems(
                                        hasAttrValue("name", "sample.js"),
                                        hasAttrValue("name", "sample.css")
                                )
                        )
                )
        ));
    }

    @Test
    public void testSoyResource() throws Exception {
        given_web_resource_builder(
                web_resource("soy-resources/file.soy")
        );

        when_i_get_elements();

        then_descriptor(
                hasXPath("/*/web-resource[1]/resource[@name='file.soy.js' and @type='download' and @location='web-resource/soy-resources/file.soy']"),
                hasXPath("/*/web-resource[1]/resource[@name='file.soy' and @type='soy' and @location='web-resource/soy-resources/file.soy']"),
                hasXPath("/*/web-resource[1]/transformation[@extension='soy']/transformer[@key='soyTransformer']")
        );
    }

    @Test
    public void testLessResource() throws Exception {
        given_web_resource_builder(
                web_resource("less-resources/file.less")
        );

        when_i_get_elements();

        then_descriptor(
                hasXPath("/*/web-resource[1]/resource[@name='file.css' and @type='download' and @location='web-resource/less-resources/file.less']"),
                hasXPath("/*/web-resource[1]/transformation[@extension='less']/transformer[@key='lessTransformer']")
        );
    }


    @Test
    public void testExcludedFiles() throws FileNotFoundException {
        given_web_resource_builder(
                web_resource("excluded-files/web-resource.xml"),
                web_resource("excluded-files/framework-min.js")
        );

        when_i_get_elements();

        then_elements(hasItems(
                hasContents(
                        hasSize(0)
                )
        ));
    }

    @Test
    public void testExpandDependency() throws Exception {
        given_web_resource_builder(
                web_resource("with-dependencies/web-resource.xml")
        );

        when_i_get_elements();

        then_descriptor(
                hasXPath("/*/web-resource[1]/dependency[1]", equalTo("another.plugin:explicit-plugin-key-dep")),
                hasXPath("/*/web-resource[1]/dependency[2]", equalTo(OUR_PLUGIN_KEY + ":implicit-plugin-key-dep"))
        );
    }

    @Test
    public void testBefore() throws Exception {
        given_web_resource_builder(
                web_resource("with-before/pre2.js"),
                web_resource("with-before/other.js"),
                web_resource("with-before/pre1.js")
        );

        when_i_get_elements();

        then_descriptor(
                hasXPath("/*/web-resource[1]/resource[1][@name='pre1.js']"),
                hasXPath("/*/web-resource[1]/resource[2][@name='pre2.js']"),
                hasXPath("/*/web-resource[1]/resource[3][@name='other.js']"),
                not(hasXPath("/*/web-resource[1]/before"))
        );
    }

    @Test
    public void testAfter() throws Exception {
        given_web_resource_builder(
                web_resource("with-after/post2.js"),
                web_resource("with-after/other.js"),
                web_resource("with-after/post1.js")
        );

        when_i_get_elements();

        then_descriptor(
                hasXPath("/*/web-resource[1]/resource[1][@name='other.js']"),
                hasXPath("/*/web-resource[1]/resource[2][@name='post1.js']"),
                hasXPath("/*/web-resource[1]/resource[3][@name='post2.js']"),
                not(hasXPath("/*/web-resource[1]/after"))
        );
    }

    @Test
    public void testBeforeAndAfter() throws Exception {
        given_web_resource_builder(
                web_resource("with-before-and-after/post1.js"),
                web_resource("with-before-and-after/pre2.js"),
                web_resource("with-before-and-after/other.js"),
                web_resource("with-before-and-after/pre1.js"),
                web_resource("with-before-and-after/post2.js")
        );

        when_i_get_elements();

        then_descriptor(
                hasXPath("/*/web-resource[1]/resource[1][@name='pre1.js']"),
                hasXPath("/*/web-resource[1]/resource[2][@name='pre2.js']"),
                hasXPath("/*/web-resource[1]/resource[3][@name='other.js']"),
                hasXPath("/*/web-resource[1]/resource[4][@name='post1.js']"),
                hasXPath("/*/web-resource[1]/resource[5][@name='post2.js']"),
                not(hasXPath("/*/web-resource[1]/before")),
                not(hasXPath("/*/web-resource[1]/after"))
        );
    }

    private String web_resource(String resource) {
        return resource;
    }

    private void then_elements(Matcher matcher) {
        assertThat("Web Resource Elements", webResourceElements, matcher);
    }

    @SafeVarargs
    private final void then_descriptor(Matcher<Node>... matchers) throws DocumentException {
        final Element root = DocumentHelper.createDocument().addElement("atlassian-plugin");
        for (Element element : webResourceElements) {
            root.addText("\n");
            root.add(element);
            root.addText("\n");
        }
        System.out.println(root.asXML());
        assertThat("atlassian-plugin descriptor", new DOMWriter().write(root.getDocument()), allOf(matchers));
    }

    private void when_i_get_elements() {
        webResourceElements = webResourcesBuilder.getElements();
    }

    private void given_web_resource_builder(String... webResourcePaths) throws FileNotFoundException {
        if (webResourcePaths.length > 0) {
            // Use first path to initialize Plugin mock - so the mock can access the web-resource.xml if it exists
            webResourcesBuilder = WebResourcesBuilder.forPlugin(getPlugin("web-resource/" + webResourcePaths[0]));
            for (String resource : webResourcePaths) {
                webResourcesBuilder.addResource("web-resource/" + resource);
            }
        } else {
            webResourcesBuilder = WebResourcesBuilder.forPlugin(getPlugin());
        }
    }

    public Plugin getPlugin() throws FileNotFoundException {
        return mock(Plugin.class);
    }

    private Plugin getPlugin(String path) throws FileNotFoundException {
        final String pathPrefix = PATH_JOINER.join(limit(PATH_SPLITTER.split(path), 2));
        final PluginWithResource pluginWithResource = new PluginWithResource("/" + pathPrefix);
        return pluginWithResource.getPlugin(OUR_PLUGIN_KEY);
    }

}
