package ut.com.adaptavist.dynamicwebmodule.util;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.apache.commons.io.FileUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.springframework.osgi.mock.MockBundle;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

import static org.apache.commons.lang.StringUtils.substringAfterLast;

/**
 * A bit hacky, but permits finding entries (resources) from the test plugin resources directory
 */
public class MockBundleWithResources extends MockBundle {

    public MockBundleWithResources(String s, Hashtable<String, String> dictionary, BundleContext bundleContext) {
        super(s, dictionary, bundleContext);
    }

    public Enumeration findEntries(String path, final String filePattern, boolean recurse) {
        Enumeration enm = null;

        try {
            final Enumeration<URL> pathEnum = this.getClass().getClassLoader().getResources(path);
            final URL parent = pathEnum.nextElement();
            final Collection collection = FileUtils.listFiles(new File(parent.toURI()), null, recurse);
            // Very simplistic filter just to match test cases of *.* and *.vm
            final Iterable filesMatchingPattern = Iterables.filter(collection, new Predicate() {
                @Override
                public boolean apply(Object input) {
                    return "*.*".equals(filePattern) || input.toString().endsWith(substringAfterLast(filePattern, "."));
                }
            });
            final Iterable urls = Iterables.transform(filesMatchingPattern, new Function() {
                @Override
                public URL apply(Object input) {
                    try {
                        return ((File) input).toURI().toURL();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
            enm = new ListEnumeration(urls);

        } catch (URISyntaxException e) {
            //System.err.println("returning an empty enumeration as cannot load resource; exception " + e);
        } catch (IOException ex) {
            //System.err.println("returning an empty enumeration as cannot load resource; exception " + ex);
        }
        return (enm == null ? new EmptyEnumeration() : enm);
    }

    @Override
    public Map getSignerCertificates(int signersType) {
        return ImmutableMap.of();
    }

    @Override
    public Version getVersion() {
        return new Version(0, 0, 1);
    }

    private static class ListEnumeration implements Enumeration {

        private final Iterator<URL> iterator;

        public ListEnumeration(Iterable<URL> list) {
            this.iterator = list.iterator();
        }

        @Override
        public boolean hasMoreElements() {
            return iterator.hasNext();
        }

        @Override
        public URL nextElement() {
            return iterator.next();
        }
    }

    private static class EmptyEnumeration implements Enumeration {

        public boolean hasMoreElements() {
            return false;
        }

        public Object nextElement() {
            throw new NoSuchElementException();
        }
    }
}
