package ut.com.adaptavist.dynamicwebmodule.util;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.plugin.Plugin;

import java.io.FileNotFoundException;
import java.io.InputStream;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Provides a mocked Plugin that will successfully load an existing web-resource.xml from a directory in the test plugin
 * resources directory.
 */
public class PluginWithResource {
    final InputStream inputStream;

    public PluginWithResource(String resourceDirectory) {
        inputStream = this.getClass().getResourceAsStream(resourceDirectory + "/web-resource.xml");
    }

    public Plugin getPlugin(String pluginKey) throws FileNotFoundException {
        final Plugin mockedPlugin = mock(Plugin.class);
        when(mockedPlugin.getResourceAsStream(anyString())).thenReturn(inputStream);
        when(mockedPlugin.getKey()).thenReturn(pluginKey);
        return mockedPlugin;
    }

}
