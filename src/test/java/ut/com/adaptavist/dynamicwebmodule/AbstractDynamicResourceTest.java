package ut.com.adaptavist.dynamicwebmodule;

/*
 * #%L
 * Dynamic Web Modules
 * %%
 * Copyright (C) 2015 Adaptavist.com Ltd
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.adaptavist.dynamicwebmodule.AbstractDynamicResourceModuleManager;
import com.adaptavist.dynamicwebmodule.DynamicWebPanelManager;
import com.adaptavist.dynamicwebmodule.DynamicWebResourceManager;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.atlassian.pocketknife.api.lifecycle.modules.ModuleRegistrationHandle;
import org.dom4j.Element;
import org.hamcrest.Matcher;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import ut.com.adaptavist.dynamicwebmodule.util.MockBundleWithResources;
import ut.com.adaptavist.dynamicwebmodule.util.PluginWithResource;

import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractDynamicResourceTest {

    protected final String PLUGIN_KEY = "test-plugin-key";
    protected AbstractDynamicResourceModuleManager resourceModuleManager;
    protected DynamicModuleDescriptorFactory moduleDescriptorFactory;
    protected Plugin plugin;

    protected void then_resource_handles(Matcher matcher) {
        and_resource_handles(matcher);
    }

    protected void and_resource_handles(Matcher matcher) {
        final Set<String> registeredModuleKeys = resourceModuleManager.getRegisteredModuleKeys();

        assertThat(registeredModuleKeys, matcher);
    }

    protected void given_dynamic_web_resource_manager() throws FileNotFoundException {
        moduleDescriptorFactory = mock(DynamicModuleDescriptorFactory.class);

        final BundleContext bundleContext = getBundleContext();
        final PluginAccessor pluginAccessor = getPluginAccessor();

        final ModuleRegistrationHandle registrationHandle = mock(ModuleRegistrationHandle.class);
        when(moduleDescriptorFactory.loadModules(eq(plugin), any(Element.class))).thenReturn(registrationHandle);

        resourceModuleManager = new DynamicWebResourceManager(
                moduleDescriptorFactory,
                pluginAccessor,
                bundleContext
        );
    }

    protected void given_dynamic_web_panel_manager() throws FileNotFoundException {
        moduleDescriptorFactory = mock(DynamicModuleDescriptorFactory.class);

        final BundleContext bundleContext = getBundleContext();
        final PluginAccessor pluginAccessor = getPluginAccessor();

        final ModuleRegistrationHandle registrationHandle = mock(ModuleRegistrationHandle.class);
        when(moduleDescriptorFactory.loadModules(eq(plugin), any(Element.class))).thenReturn(registrationHandle);

        resourceModuleManager = new DynamicWebPanelManager(
                moduleDescriptorFactory,
                pluginAccessor,
                bundleContext
        );
    }

    private PluginAccessor getPluginAccessor() throws FileNotFoundException {
        final PluginAccessor pluginAccessor = mock(PluginAccessor.class);
        final PluginWithResource pluginWithResource = new PluginWithResource("/web-resource/without-xml");
        plugin = pluginWithResource.getPlugin(PLUGIN_KEY);
        when(pluginAccessor.getPlugin(PLUGIN_KEY)).thenReturn(plugin);
        return pluginAccessor;
    }

    private BundleContext getBundleContext() {
        final Hashtable<String, String> dictionary = new Hashtable<>();
        dictionary.put(OsgiPlugin.ATLASSIAN_PLUGIN_KEY, PLUGIN_KEY);

        final Bundle bundle = new MockBundleWithResources("Test Plugin Bundle", dictionary, null);
        return bundle.getBundleContext();
    }

}
