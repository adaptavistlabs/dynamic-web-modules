# Dynamic Web Modules

Your plugin should already be using the [Atlassian spring scanner](https://bitbucket.org/atlassian/atlassian-spring-scanner),
and the Adaptavist (or similar) [base pom](https://bitbucket.org/Adaptavist/adaptavist-base-plugin-pom).

## Getting started

### Maven dependencies

Add these dependencies to your POM - we need them to be compiled in:  
```
<dependency>
    <groupId>com.adaptavist</groupId>
    <artifactId>dynamic-web-modules</artifactId>
    <version>2.0.1</version>
    <scope>compile</scope>
</dependency>
<dependency>
    <groupId>com.atlassian.pocketknife</groupId>
    <artifactId>atlassian-pocketknife-dynamic-modules</artifactId>
    <version>1.0</version>
    <scope>compile</scope>
</dependency>
```
or, if you are using the Adaptavist base pom:
```
<dependency>
    <groupId>com.adaptavist</groupId>
    <artifactId>dynamic-web-modules</artifactId>
</dependency>
<dependency>
    <groupId>com.atlassian.pocketknife</groupId>
    <artifactId>atlassian-pocketknife-dynamic-modules</artifactId>
</dependency>
```
if you want to override the version from the base-pom, you can set a property:
```
<properties>
    <adaptavist.dynamic.web.modules.version>2.0.1</adaptavist.dynamic.web.modules.version>
</properties>
```

### Adaptavist Maven Repository

If you are using this in a non-Adaptavist project you'll need to include
our external maven repository too:
```
<pluginRepository>
    <id>adaptavist-external</id>
    <url>https://nexus.adaptavist.com/content/repositories/external</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</pluginRepository>
```

### Spring scanner configuration

Update your Spring Scanner configuration so that this module will be scanned:  
```
<plugin>
    <groupId>com.atlassian.plugin</groupId>
    <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
    <configuration>
        <includeExclude>+com.adaptavist.*,+com.atlassian.pocketknife.*</includeExclude>
        <scannedDependencies>
            <dependency>
                <groupId>com.adaptavist</groupId>
                <artifactId>dynamic-web-modules</artifactId>
            </dependency>
            <dependency>
                <groupId>com.atlassian.pocketknife</groupId>
                <artifactId>atlassian-pocketknife-dynamic-modules</artifactId>
             </dependency>
        </scannedDependencies>
    </configuration>
</plugin>
```

## Web Resources

Add a 'web-resource' folder under src/main/resources (or src/main/frontend if you are using the frontend maven plugin),
and create a folder under that for each web-resource module that you want to create.

You can then dump whatever files you want into that, ie. js, css, images, and they will automatically be added to the
web-resource.

You can also add a template web-resource.xml file to allow you to set key, name, contexts and dependencies - without
which the key will default to 'web-resource-<folder-name>'.

### Additional web-resource features

The web-resource.xml provide some additional features beyond the usual module found in the atlassian-plugin.xml

#### Implicit plugin key in dependencies

To avoid having to hard-code your plugin key into <dependency> elements you can use a short form of module key:

    <dependency>:my-module</dependency>

#### Ordering of resources

Due to the fact that resources are added dynamically, the ordering of them (which is sometimes important) could not be
controlled. So we've added placeholders to specify whether a resource should appear before or after all other resources.

    <web-resource>
        <before>include-me-first.js</before>
        <before>include-me-second.js</before>
        <after>include-me-last.js</after>
    </web-resource>

This would ensure that the include-me-first.js & include-me-second.js files appear in that order and before all other
dynamic resources, and the include-me-last.js appear after them.

NOTE: The name inside these placeholders represents the name attribute generated for the resource element, so for
transformed files it may not be the original filename.

#### Transformed files

If a soy or less file is found, the appropriate transformer will automatically be added to the web-resource.

Also, for soy files, resources are added for both download as JS and use in server-side rendering.

## Web Panels

Add a 'web-panel' folder under src/main/resources (or src/main/frontend if you are using the frontend maven plugin),
and create a folder under that for each web-panel module that you want to create.

You can then dump Velocity files into that, ie. vm files and they will automatically be added to the web-panel.

## License

[Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0)

## Changelog

### 2.0.1

* Update spring scanner
* Deploy to publicly accessible maven repo

### 2.0.0

* Remove directory watchers, which didn't work correctly anyway - to be replaced by an alternative external solution
* Added before/after placeholders
* Added prepending of plugin key to implicit module keys in dependencies
* Support for server-side soy resources

### 1.0.1

* Moved WebResourcesBuilder to top-level class
* Added support for watching of directories and reloaded dynamic modules
* Unit tests added

### 1.0.0

* Initial Release
